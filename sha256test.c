
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "sha256.h"

#ifndef TEST_MAIN
# define testMain
#endif


void test(const char *expected, const char *data)
{
    SHA256_HASH hash;

    sha256_hash_string(hash, data);

    char s[65];
    sha256_hash_to_string(hash, s);

    if ( strcmp(s, expected) != 0 )
    {
        printf("Unexpected result of \'%s\':\n", data);
        printf("%s\n", s);
    }
}


int TEST_MAIN()
{
    test("e3b0c44298fc1c149afbf4c8996fb92427ae41e4649b934ca495991b7852b855", "");
    test("ca978112ca1bbdcafac231b39a23dc4da786eff8147c4e72b9807785afee48bb", "a");
    test("ba7816bf8f01cfea414140de5dae2223b00361a396177a9cb410ff61f20015ad", "abc");
    test("248d6a61d20638b8e5c026930c3e6039a33ce45964ff2167f6ecedd419db06c1", "abcdbcdecdefdefgefghfghighijhijkijkljklmklmnlmnomnopnopq");
    test("cf5b16a778af8380036ce59e7b0492370b249b11e8f07a51afac45037afee9d1", "abcdefghbcdefghicdefghijdefghijkefghijklfghijklmghijklmnhijklmnoijklmnopjklmnopqklmnopqrlmnopqrsmnopqrstnopqrstu");

    return (0);
}

