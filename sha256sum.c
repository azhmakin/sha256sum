#include <stdio.h>
#include <stdlib.h>

#include "sha256.h"

#ifndef SUM_MAIN
# define sumMain
#endif


void process_file(const char *filename)
{
    SHA256_HASH hash;

    sha256_hash_file(hash, filename);

    char s[65];
    sha256_hash_to_string(hash, s);

    printf("%s %s\n", s, filename);
}


int SUM_MAIN(int argc, char *argv[])
{
    if (argc <= 1)
    {
        SHA256_HASH hash;

        sha256_hash_stream(hash, stdin);

        char s[65];
        sha256_hash_to_string(hash, s);

        printf("%s\n", s);
    }
    else
    {
        for (int i = 1; i < argc; i++)
        {
            process_file(argv[i]);
        }
    }

    return (0);
}
