
#include <stdio.h>
#include <assert.h>
#include "sha256.h"


void sha256_init(SHA256_HASH hash)
{
    hash[0] = UINT32_C(0x6a09e667);
    hash[1] = UINT32_C(0xbb67ae85);
    hash[2] = UINT32_C(0x3c6ef372);
    hash[3] = UINT32_C(0xa54ff53a);
    hash[4] = UINT32_C(0x510e527f);
    hash[5] = UINT32_C(0x9b05688c);
    hash[6] = UINT32_C(0x1f83d9ab);
    hash[7] = UINT32_C(0x5be0cd19);
}


static const uint32_t k[] = {
    UINT32_C(0x428a2f98), UINT32_C(0x71374491), UINT32_C(0xb5c0fbcf), UINT32_C(0xe9b5dba5),
    UINT32_C(0x3956c25b), UINT32_C(0x59f111f1), UINT32_C(0x923f82a4), UINT32_C(0xab1c5ed5),
    UINT32_C(0xd807aa98), UINT32_C(0x12835b01), UINT32_C(0x243185be), UINT32_C(0x550c7dc3),
    UINT32_C(0x72be5d74), UINT32_C(0x80deb1fe), UINT32_C(0x9bdc06a7), UINT32_C(0xc19bf174),
    UINT32_C(0xe49b69c1), UINT32_C(0xefbe4786), UINT32_C(0x0fc19dc6), UINT32_C(0x240ca1cc),
    UINT32_C(0x2de92c6f), UINT32_C(0x4a7484aa), UINT32_C(0x5cb0a9dc), UINT32_C(0x76f988da),
    UINT32_C(0x983e5152), UINT32_C(0xa831c66d), UINT32_C(0xb00327c8), UINT32_C(0xbf597fc7),
    UINT32_C(0xc6e00bf3), UINT32_C(0xd5a79147), UINT32_C(0x06ca6351), UINT32_C(0x14292967),
    UINT32_C(0x27b70a85), UINT32_C(0x2e1b2138), UINT32_C(0x4d2c6dfc), UINT32_C(0x53380d13),
    UINT32_C(0x650a7354), UINT32_C(0x766a0abb), UINT32_C(0x81c2c92e), UINT32_C(0x92722c85),
    UINT32_C(0xa2bfe8a1), UINT32_C(0xa81a664b), UINT32_C(0xc24b8b70), UINT32_C(0xc76c51a3),
    UINT32_C(0xd192e819), UINT32_C(0xd6990624), UINT32_C(0xf40e3585), UINT32_C(0x106aa070),
    UINT32_C(0x19a4c116), UINT32_C(0x1e376c08), UINT32_C(0x2748774c), UINT32_C(0x34b0bcb5),
    UINT32_C(0x391c0cb3), UINT32_C(0x4ed8aa4a), UINT32_C(0x5b9cca4f), UINT32_C(0x682e6ff3),
    UINT32_C(0x748f82ee), UINT32_C(0x78a5636f), UINT32_C(0x84c87814), UINT32_C(0x8cc70208),
    UINT32_C(0x90befffa), UINT32_C(0xa4506ceb), UINT32_C(0xbef9a3f7), UINT32_C(0xc67178f2)
};


#define RIGHT_ROTATE(x,s) ( ( (x) >> (s) ) | ( (x) << (32 - (s)) ) )


void sha256_process_chunk(SHA256_HASH hash, SHA256_CHUNK chunk)
{
    uint32_t w[64];

    for (int i = 0; i < 16; ++i)
    {
        w[i] = chunk[i];
    }

    for (int i = 16; i < 64; ++i)
    {
        uint32_t s0 = RIGHT_ROTATE(w[i - 15],  7) ^ RIGHT_ROTATE(w[i - 15], 18) ^ (w[i - 15] >>  3);
        uint32_t s1 = RIGHT_ROTATE(w[i -  2], 17) ^ RIGHT_ROTATE(w[i -  2], 19) ^ (w[i -  2] >> 10);
        w[i] = w[i-16] + s0 + w[i-7] + s1;
    }

    uint32_t a = hash[0];
    uint32_t b = hash[1];
    uint32_t c = hash[2];
    uint32_t d = hash[3];
    uint32_t e = hash[4];
    uint32_t f = hash[5];
    uint32_t g = hash[6];
    uint32_t h = hash[7];

    for (int i = 0; i < 64; ++i)
    {
        uint32_t S1 = RIGHT_ROTATE(e, 6) ^ RIGHT_ROTATE(e, 11) ^ RIGHT_ROTATE(e, 25);
        uint32_t ch = (e & f) ^ ((~e) & g);
        uint32_t temp1 = h + S1 + ch + k[i] + w[i];
        uint32_t S0 = RIGHT_ROTATE(a, 2) ^ RIGHT_ROTATE(a, 13) ^ RIGHT_ROTATE(a, 22);
        uint32_t maj = (a & b) ^ (a & c) ^ (b & c);
        uint32_t temp2 = S0 + maj;

        h = g;
        g = f;
        f = e;
        e = d + temp1;
        d = c;
        c = b;
        b = a;
        a = temp1 + temp2;
    }

    hash[0] += a;
    hash[1] += b;
    hash[2] += c;
    hash[3] += d;
    hash[4] += e;
    hash[5] += f;
    hash[6] += g;
    hash[7] += h;
}


static void append_padding(SHA256_CHUNK chunk, int index)
{
    int limit = (index < 448) // 511-64+1
            ? 14
            : 16;

    int iW = index / 32;
    int iB = 31 - index % 32;

    chunk[iW] |= UINT32_C(1) << iB;
    chunk[iW] &= (~UINT32_C(0)) << iB;

    for (int i = iW + 1; i < limit; i++)
    {
        chunk[i] = 0;
    }
}


void sha256_finalize(SHA256_HASH hash, SHA256_CHUNK chunk, uint64_t message_length)
{
    int finalBits = message_length % 512;

    int emptyBits = 512 - finalBits;

    // We have enough space in this chunk to complete computation
    if (emptyBits >= 65)
    {
        append_padding(chunk, finalBits);

        chunk[14] = (message_length >> 32) & UINT32_C(0xFFFFFFFF);
        chunk[15] = (message_length      ) & UINT32_C(0xFFFFFFFF);

        sha256_process_chunk(hash, chunk);
    }
    else
    {
        if (emptyBits > 0)
        {
            append_padding(chunk, finalBits);

            sha256_process_chunk(hash, chunk);
        }

        SHA256_CHUNK temp;

        if (emptyBits == 0)
        {
            temp[0] = UINT32_C(0x80000000);
        }
        else
        {
            temp[0] = UINT32_C(0);
        }

        for (int i = 1; i < 14; i++)
        {
            temp[i] = UINT32_C(0);
        }

        temp[14] = (message_length >> 32) & UINT32_C(0xFFFFFFFF);
        temp[15] = (message_length      ) & UINT32_C(0xFFFFFFFF);

        sha256_process_chunk(hash, temp);
    }
}


void sha256_block_to_chunk(uint8_t *block, SHA256_CHUNK chunk)
{
    for (int i = 0; i < 64; i++)
    {
        if (i % 4 == 0)
        {
            chunk[i / 4] = ((uint32_t) block[i]) << (3 * 8);
        }
        else
        {
            chunk[i / 4] |= ((uint32_t) block[i]) << ((3 - (i % 4)) * 8);
        }
    }
}


// TODO: Describe the order of writing!!!
static void set_byte_in_chunk(SHA256_CHUNK chunk, uint8_t value, int index)
{
    assert(index >= 0 && index < 64);

    int iW = index / 4;
    int iB = index % 4;

    if (iB == 0)
    {
        chunk[iW] = (uint32_t) value << 24;
    }
    else
    {
        chunk[iW] |= (uint32_t) value << ((3 - iB) * 8);
    }
}


void sha256_hash_string(SHA256_HASH hash, const char *str)
{
    SHA256_CHUNK chunk;

    sha256_init(hash);

    uint64_t index = 0;

    while (str[index] != '\0')
    {
        char c = str[index];

        set_byte_in_chunk(chunk, c, index % 64);

        index++;

        if (index % 64 == 0)
        {
            sha256_process_chunk(hash, chunk);
        }
    }

    sha256_finalize(hash, chunk, index * 8);
}


int sha256_hash_file(SHA256_HASH hash, const char *filename)
{
    FILE *f;

    f = fopen(filename, "rb");

    if (f == NULL)
    {
        return (1);
    }

    return sha256_hash_stream(hash, f);
}


int sha256_hash_stream(SHA256_HASH hash, FILE *stream)
{
    SHA256_CHUNK chunk;

    sha256_init(hash);

    uint64_t index = 0;

    int c;

    while ( (c = getc(stream)) != -1 )
    {
        set_byte_in_chunk(chunk, c, index % 64);

        index++;

        if (index % 64 == 0)
        {
            sha256_process_chunk(hash, chunk);
        }
    }

    fclose(stream);

    sha256_finalize(hash, chunk, index * 8);

    return (0);
}


void sha256_hash_to_string(SHA256_HASH hash, char *str)
{
    const static char digit[] = {'0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'a', 'b', 'c', 'd', 'e', 'f'};

    int index = 0;

    for (int i = 0; i < 8; i++)
    {
        for (int j = 32; (j -= 4) >= 0; index++)
        {
            str[index] = digit[ (hash[i] >> j) & 0x0F ];
        }
    }

    str[64] = '\0';
}

