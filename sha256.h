
#pragma once

#include <stdint.h>

typedef uint32_t SHA256_HASH[8]; // 256 bits, 32 bytes
typedef uint32_t SHA256_CHUNK[16]; // 512 bits, 64 bytes

void sha256_init(SHA256_HASH hash);

void sha256_process_chunk(SHA256_HASH hash, SHA256_CHUNK chunk);

/**
 * chunk -- The last non-complete chunk of the message. If |message| % 512 == 0, then this chunk is treated as empty.
 *          This function will write data (padding) into the bits it considers empty. Significant bits will never be overwritten though.
 * message_length -- Message length in bits.
 */
void sha256_finalize(SHA256_HASH hash, SHA256_CHUNK chunk, uint64_t message_length);

void sha256_hash_string(SHA256_HASH hash, const char *str);

int sha256_hash_file(SHA256_HASH hash, const char *filename);

int sha256_hash_stream(SHA256_HASH hash, FILE *stream);

void sha256_hash_to_string(SHA256_HASH hash, char *str);

void sha256_block_to_chunk(uint8_t *block, SHA256_CHUNK chunk);
